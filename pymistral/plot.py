import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from matplotlib.ticker import MaxNLocator

import cartopy as cp
import cartopy.crs as ccrs
from cartopy.mpl.ticker import LatitudeFormatter, LongitudeFormatter


@xr.register_dataarray_accessor('plot_map')
class CartopyMap(object):
    """
    Plot the given 2D array on a cartopy axes with
     ('lon','longitude' or any coord with 'longitude' as standard_name) assumed as Longitude and
     ('lat','latitude' or any coord with 'latitude' as standard_name) assumed as Latitude.
    The default projection is PlateCarree, but can be:
        cartopy.crs.<ProjectionName>()
    If you would like to create a figure with multiple subplots
    you can pass an axes object to the function with keyword argument `ax,
    BUT then you need to specify the projection when you create the axes:
        plt.axes([x0, y0, w, h], projection=cartopy.crs.<ProjectionName>())
    Additional keywords can be given to the function as you would to
    the xr.DataArray.plot function. The only difference is that `robust`
    is set to True by default.
    The function returns a GeoAxes object to which features can be added with:
        ax.add_feature(feature.<FeatureName>, **kwargs)
    """

    def __init__(self, xarray_obj):
        self._obj = xarray_obj

    def __call__(
        self,
        ax=None,
        proj=None,
        draw_lon_lat_labels=True,
        feature=None,
        plot_type='pcolormesh',
        rm_cyclic=True,
        **kwargs,
    ):
        return self._cartopy(
            ax=ax,
            proj=proj,
            feature=feature,
            draw_lon_lat_labels=draw_lon_lat_labels,
            plot_type=plot_type,
            rm_cyclic=rm_cyclic,
            **kwargs,
        )

    def _cartopy(
        self,
        ax=None,
        proj=ccrs.PlateCarree(),
        feature='land',
        draw_lon_lat_labels=True,
        plot_type='pcolormesh',
        rm_cyclic=True,
        **kwargs,
    ):

        # TODO: CESM2 and GFDL-ESM4 have lon issue

        if not proj:
            proj = ccrs.PlateCarree()
        if isinstance(proj, str):
            proj = eval(f'ccrs.{proj}()')

        stereo_maps = (
            ccrs.Stereographic,
            ccrs.NorthPolarStereo,
            ccrs.SouthPolarStereo,
        )
        if isinstance(proj, stereo_maps):
            round = True
            import matplotlib.path as mpath

            theta = np.linspace(0, 2 * np.pi, 100)
            center, radius = [0.5, 0.5], 0.5
            verts = np.vstack([np.sin(theta), np.cos(theta)]).T
            circle = mpath.Path(verts * radius + center)
        else:
            round = False

        xda = self._obj
        # da, convert to da or error
        if not isinstance(xda, xr.DataArray):
            if len(xda.data_vars) == 1:
                xda = xda[xda.data_vars[0]]
            else:
                raise ValueError(
                    f'Please provide xr.DataArray, found {type(xda)}'
                )

        assert (
            (xda.ndim == 2)
            or (xda.ndim == 3 and 'col' in kwargs or 'row' in kwargs)
            or (xda.ndim == 4 and 'col' in kwargs and 'row' in kwargs)
        ), (xda.ndim, xda.dims)
        single_plot = True if xda.ndim == 2 else False

        # find whether curv or not
        curv = False
        lon = None
        lat = None
        for c in xda.coords:
            if c in ['xc','lon', 'longitude']:
                lon = c
            else:
                for attr, value in zip(['standard_name','long_name','units'],['longitude','longitude','degrees_east']):
                    if attr in xda[c].attrs:
                        if value in xda[c].attrs[attr]:
                            lon = c

            if c in ['yc', 'lat', 'latitude']:
                lat = c
            else:
                for attr, value in zip(['standard_name','long_name','units'],['latitude','latitude','degrees_north']):
                    if attr in xda[c].attrs:
                        if value in xda[c].attrs[attr]:
                            lat = c

        assert lon, ('did not find longitude', lon, xda.coords)
        assert lat, ('did not find latitude', lat, xda.coords)

        if len(xda[lon].dims) == 2 and len(xda[lat].dims) == 2:
            curv = True

        
        if draw_lon_lat_labels:
            draw_labels = True
        else:
            draw_labels = False
        if proj != ccrs.PlateCarree():
            draw_lon_lat_labels = False

        if plot_type == 'contourf':
            rm_cyclic = False
        if curv and rm_cyclic:
            xda = _rm_singul_lon(xda, lon=lon, lat=lat)

        if 'robust' not in kwargs:
            kwargs['robust'] = True
        if 'cbar_kwargs' not in kwargs:
            kwargs['cbar_kwargs'] = {'shrink': 0.6}

        if ax is None:
            if round:
                print(
                    'use stereo maps and facet plots by:',
                    ' fig,ax=plt.subplots(subplot_kw=',
                    '{"projection":ccrs.NorthPolarStereo()}\n',
                    'd.isel(time=0).plot_map(proj=proj,ax=ax)',
                )
            if single_plot:
                axm = getattr(xda.plot, plot_type)(
                    lon,
                    lat,
                    ax=plt.axes(projection=proj),
                    transform=ccrs.PlateCarree(),
                    **kwargs,
                )
            else:
                axm = getattr(xda.plot, plot_type)(
                    lon,
                    lat,
                    subplot_kws={'projection': proj},
                    transform=ccrs.PlateCarree(),
                    **kwargs,
                )
        else:
            if round:
                ax.set_boundary(circle, transform=ax.transAxes)
            axm = getattr(xda.plot, plot_type)(
                lon, lat, ax=ax, transform=ccrs.PlateCarree(), **kwargs
            )

        def work_on_axes(axes):
            if 'coastline_color' in kwargs:
                coastline_color = kwargs['coastline_color']
            else:
                coastline_color = 'gray'
            axes.coastlines(color=coastline_color, linewidth=1.5)
            if feature is not None:
                axes.add_feature(
                    getattr(cp.feature, feature.upper()),
                    zorder=100,
                    edgecolor='k',
                )

            if draw_lon_lat_labels:
                _set_lon_lat_axis(axes, proj)
            else:
                gl = axes.gridlines(draw_labels=draw_labels)
                gl.top_labels = False
                gl.right_labels = False
                if not isinstance(proj, stereo_maps):
                    gl.xlines = False
                    gl.ylines = False

        if single_plot:
            if ax is None:
                ax = plt.gca()
            work_on_axes(ax)
        else:
            for axes in axm.axes.flat:
                work_on_axes(axes)
        return axm


def _rm_singul_lon(ds, lon='lon', lat='lat'):
    """Remove singularity from coordinates.

    http://nbviewer.jupyter.org/gist/pelson/79cf31ef324774c97ae7
    """
    lons = ds[lon].values
    fixed_lons = lons.copy()
    for i, start in enumerate(np.argmax(np.abs(np.diff(lons)) > 180, axis=1)):
        fixed_lons[i, start + 1 :] += 360
    lons_da = xr.DataArray(fixed_lons, ds[lat].coords)
    ds = ds.assign_coords({lon: lons_da})
    return ds


def _set_lon_lat_axis(ax, projection, talk=False):
    """Add longitude and latitude coordinates to cartopy plots."""
    ax.set_xticks([-180, -120, -60, 0, 60, 120, 180], crs=projection)
    ax.set_yticks([-60, -30, 0, 30, 60, 90], crs=projection)
    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    ax.set_xlabel('')
    ax.set_ylabel('')
    if talk:
        ax.outline_patch.set_edgecolor('black')
        ax.outline_patch.set_linewidth('1.5')
        ax.tick_params(labelsize=15)
        ax.tick_params(width=1.5)


def _set_integer_xaxis(ax):
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))

    

def rad_to_deg(ds):
    """Convert radian units to deg."""
    #ds.coords.compute()
    with xr.set_options(keep_attrs=True):
        for c in ds.coords:
            if 'units' in ds[c].attrs:
                if ds[c].attrs['units'] == 'radian':
                    print(f'convert {c} from rad to deg')
                    ds[c] = ds[c]* 180./np.pi
                    ds[c].attrs['units'] = 'degrees'
            elif 'bnds' in c:
                print(f'convert {c} from rad to deg')
                ds[c] = ds[c]* 180./np.pi
                ds[c].attrs['units'] = 'degrees'
    return ds    

@xr.register_dataset_accessor('icon')
class CartopyMap(object):
    def __init__(self, xarray_obj):
        self._obj = xarray_obj

    def plot(self,
             v=None,
             ax=None,
             proj=ccrs.PlateCarree(),
             robust=False,
             feature=None,
             mask=True,
             lon='clon',
             lat='clat',
             lon_bnds='clon_bnds',
             lat_bnds='clat_bnds',
             **tripcolor_kwargs
        ):
        """
        Plot the variable v from an xr.Dataset.
        
        Note: xr.DataArray.icon.plot() would be nicer,
        but the xr.DataArray doesnt carry the neccessary clon_bnds and clat_bnds coords.
        
        Ideally, this plot function could work like xr.DataArray.plot(col, row, ...)
        
        Example:
        
        >>> ds.icon.plot('sst', feature='land')
        """
        ds = self._obj
        if 'clon_bnds' in ds.data_vars and 'clat_bnds' in ds.data_vars:
            ds = ds.set_coords(['clon_bnds','clat_bnds'])
        ds = rad_to_deg(ds)
        
        # --- Triangulation
        ntr = ds[lon].size
        clon_bnds_rs = ds[lon_bnds].values.reshape(ntr*3)
        clat_bnds_rs = ds[lat_bnds].values.reshape(ntr*3)
        triangles = np.arange(ntr*3).reshape(ntr,3)
        Tri = matplotlib.tri.Triangulation(clon_bnds_rs, clat_bnds_rs, triangles=triangles)
        if mask: # mask big triangles https://gitlab.dkrz.de/m300602/pyicon/-/blob/master/pyicon/pyicon_tb.py#L966
            mask_bt = (
                  (np.abs(  clon_bnds_rs[triangles[:,0]] 
                          - clon_bnds_rs[triangles[:,1]])>180.)
                | (np.abs(  clon_bnds_rs[triangles[:,0]] 
                          - clon_bnds_rs[triangles[:,2]])>180.)
                            )
            Tri.set_mask(mask_bt)


        if isinstance(ds,xr.Dataset):
            if v is None:
                v = list(ds.data_vars)
                if len(v) > 0:
                    v = v[0] # take first variable
                    da = ds[v]
            else:
                da = ds[v]

        vmin = tripcolor_kwargs.pop('vmin',None)
        vmax = tripcolor_kwargs.pop('vmax',None)
        if vmin is None and vmax is None and robust:
            vmin = da.quantile(0.02).values
            vmax = da.quantile(.98).values
        
        # what if more than two dims: see xr.facetgrid
        fig, axes = plt.subplots(subplot_kw=dict(projection=proj) if proj else None,figsize=(8,4))
        # best would be to add tripcolor to xarray
        tripcolor_kwargs.update({'vmin':vmin, 'vmax':vmax})
        if proj:
            tripcolor_kwargs.update({'transform':ccrs.PlateCarree()})
        hm = axes.tripcolor(Tri,
                            da,
                            **tripcolor_kwargs
                            )
        # now add features, labels etc
        if feature and proj:
            axes.add_feature(getattr(cp.feature,feature.upper()), zorder=0)
        #axes.set_extent((-178, 178, -88, 88), crs=ccrs.PlateCarree())
        cb = plt.colorbar(mappable=hm, ax=axes, extend='both')
        ## use metadata
        title_str = ''
        title_labels = [c for c in da.coords if da.coords[c].size == 1]
        for l in title_labels:
            title_str += f', {l} = {str(da[l].astype(str).values)}'
        axes.set_title(title_str[1:].replace("T00:00:00.000000000",""))
        cb.set_label(f'{da.attrs["long_name"]}\n[{da.attrs["units"]}]', rotation = 90)
        # add x,y ticks and labels
        return axes