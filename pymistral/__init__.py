"""
pymistral
--------

A package for analyzing, processing, and mapping ESM output on mistral.

Available Modules
-----------------
- setup
- plot: Cartopy Mapping for MPIOM curvilinear grids
- hamocc: HAMOCC-specific helper functions
- slurm_post: write python code to file and send to SLURM (experimental)
- cdo_post: postprocessing with CDO into xarray
"""
try:
    from . import cdo_post, setup
    from .setup import cdo
except ImportError:
    pass
from . import hamocc, plot, slurm_post

