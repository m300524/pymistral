import pytest
import xarray as xr


@pytest.fixture
def rasm_da():
    return xr.tutorial.load_dataset('rasm')['Tair']


@pytest.fixture
def air_temperature_da():
    return xr.tutorial.load_dataset('air_temperature')['air']
