import pytest
import pymistral
import cartopy.crs as ccrs
import matplotlib.pyplot as plt

projections = [ccrs.PlateCarree(), ccrs.Robinson()]
plot_types = ['pcolormesh', 'contourf']


@pytest.mark.parametrize(
    'da',
    [
        pytest.lazy_fixture('rasm_da'),
        pytest.lazy_fixture('air_temperature_da'),
    ],
)
@pytest.mark.parametrize('plot_type', plot_types)
@pytest.mark.parametrize('proj', projections)
def test_plot_map_proj(da, plot_type, proj):
    da.isel(time=0).plot_map(proj=proj)


@pytest.mark.parametrize(
    'da',
    [
        pytest.lazy_fixture('rasm_da'),
        pytest.lazy_fixture('air_temperature_da'),
    ],
)
@pytest.mark.parametrize('rc', ['col', 'row'])
@pytest.mark.parametrize('plot_type', plot_types)
@pytest.mark.parametrize('proj', projections)
def test_plot_map_facet(da, rc, plot_type, proj):
    if rc == 'col':
        da.isel(time=[0, 1]).plot_map(
            col='time', plot_type=plot_type, proj=proj
        )
    else:
        da.isel(time=[0, 1]).plot_map(
            row='time', plot_type=plot_type, proj=proj
        )


@pytest.mark.parametrize(
    'da',
    [
        pytest.lazy_fixture('rasm_da'),
        pytest.lazy_fixture('air_temperature_da'),
    ],
)
@pytest.mark.parametrize('projstr', [None, 'Robinson', 'AlbersEqualArea'])
def test_plot_map_projstr(da, projstr):
    da.isel(time=0).plot_map(proj=projstr)


@pytest.mark.parametrize(
    'da',
    [
        pytest.lazy_fixture('rasm_da'),
        pytest.lazy_fixture('air_temperature_da'),
    ],
)
@pytest.mark.parametrize(
    'projstr', ['NorthPolarStereo', 'SouthPolarStereo', 'Stereographic']
)
def test_plot_map_stereo(da, projstr):
    proj = eval(f'ccrs.{projstr}()')
    fig, ax = plt.subplots(subplot_kw={'projection': proj})
    da.isel(time=0).plot_map(proj=projstr, ax=ax)
