# Managing papers you are working with

- Do you read many papers, but have trouble finding your copy in that large stack of paper printouts after a year?
- If you finally found it, do you often gaze at a printout without notes?
- Or when you just quickly found to find that one sentence in a paper, you do not find it because your comments are only in the hard copy?
- Do you often switch between reading papers from printouts and your computer?

## My solution: iPad + PDF Expert + sharecloud ( + zotero)

I solved these issues for myself by annotation the papers I read digitally. Either directly on the computer with the mouse, or with the Apple pencil when reading on my iPad. I store all papers in a [`owncloud`](https://info.gwdg.de/dokuwiki/doku.php?id=de:services:storage_services:own_cloud:start) (MPG accounts have 50GB free) and use [PDF Expert](https://readdle.com/) for iPad annotations. PDF Expert accesses the `.pdf`  files via `webdav` from `owncloud`. PDF Expert costs 10 Euro. If you found a well-working free and open-source solution, please send me an email.

On my local laptop I use `zotero` to manage papers. Many browsers have a [plugin](https://www.zotero.org/support/adding_items_to_zotero) to store a paper to `zotero` and save the `.pdf` to the owncloud synced folder with one click.

## Work flow

This workflow enables me to integrate papers quickly into my library of papers in `zotero`, work on them with comments, annotations and drawings with a pen and access all these papers any time with my iPad and laptop.