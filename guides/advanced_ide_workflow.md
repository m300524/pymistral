# Advanced project workflow with JupyterLab and Atom


## Enabeling remote editing with Atom
[Atom](atom.io) offers a lot of useful packages that make it (as of now) a far better tool to write clean code compared to jupyterlabs editor. In order to edit files on remote directories, we will mount them locally using `sshfs` [DKRZ explanation](https://www.dkrz.de/up/help/faq/mistral/how-to-use-sshfs-to-mount-remote-lustre-filesystem-over-ssh).
For Mac users, install [homebrew](https://brew.sh/), before installing `sshfs`.

```
$ brew install sshfs
```

This might require [`macfuse`](https://osxfuse.github.io/).

Now create a folder for your mounted drives (here we will use the homefolder)
```
$ mkdir -p ~/mistral_home
```
and mount your project directory with
```
$ sshfs m300524@mistralpp.dkrz.de:/work/ ~/mistral_home
```
I set this as an `alias` in my `~/.bash_profile`:
```
alias sshfs_home="sshfs m300524@mistralpp.dkrz.de:/work/ mistral_home -o auto_cache,reconnect,defer_permissions,noappledouble"
```

Now you can navigate to that folder as usual and start atom with `atom .`.


If you need to unmount use
```
$ umount -f ~/mistral_home
```


## Recommended atom packages

- [atom-beautify](https://atom.io/packages/atom-beautify): automatic code cleaner
- [hydrogen](https://atom.io/packages/hydropgen): jupyter inside scripts
- [teletype](https://atom.io/packages/teletype): remote peer programming
- [kite](https://atom.io/packages/kite): autocomplete, privacy dubious
- [minimap](https://atom.io/packages/minimap): sidebar of your location in code
- [linter-flake8](https://atom.io/packages/linter-flake8): automatic linting for python
- ... many more, checkout: https://atom.io/packages
